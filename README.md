# Voice-driven interaction in XR spaces

VOXReality is an initiative that aims to facilitate the convergence of Natural Language Processing (NLP) and Computer Vision (CV) technologies in the Extended Reality (XR) field. We develop innovative Artificial Intelligence (AI) models that combine language as a core interaction medium supported by visual understanding to deliver next-generation applications that provide comprehension of users’ goals, surrounding environment and context.

VOXReality’s goal is to conduct research and develop new AI models to drive future XR interactive experiences, and to deliver these models to the wider European market. These new models can address human-to-human interaction in unidirectional and bidirectional settings, as well as human-to-machine interaction.

VOXReality develops large-scale models that can be fine-tuned to specific downstream tasks with minimal re-training. At the same time, we rely on modern training approaches for designing models that include subnetworks with a common representation power but are more targeted towards specific architectures. By leveraging the once-model-for-all concept from the model training and deployment perspective, we will be able to provide a catalogue of highly generic models with high representation capacity that will be efficiently specialized for downstream tasks.

# Acknowledgement

<img src="https://s3.eu-central-1.amazonaws.com/storage.arab-reform.net/ari/2019/02/28113849/EN-V-Funded-by-the-EU_POS.png"  width="60"> VOXReality is funded by the European Union under grant agreement No. 101070521. Views and opinions expressed are however those of the author(s) only and do not necessarily reflect those of the European Union or Directorate-General for Communications Networks, Content and Technology. Neither the European Union nor the granting authority can be held responsible for them.
